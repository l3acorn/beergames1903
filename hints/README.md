# Hints

## Largest and Smallest
<details>
<summary>Click to expand</summary>

There's no hint for this one, don't think about it too much.

</details>

## Missing in Array
<details>
<summary>Click to expand</summary>

Per basic algebra, the sum, `S`, of values `1` to `n` can be calculated,                        
```
S = n * ( n + 1 ) / 2
```
Therefore, we can determine a _missing_ value by assuming the sum of the input range, and subtracting the sum of all input values.

</details>

## XOR Cipher
<details>
<summary>Click to expand</summary>

Not really a hint, but it's noteworthy that with an XOR cipher, `key XOR ciphertext = plaintext`, and by the commutative property of XOR, `plaintext XOR ciphertext = key`.

</details>

## Duplicate in Array                                                           
<details>                                                                       
<summary>Click to expand</summary>                                              
                                                                                
There are multiple approaches to this problem, it will be interesting to see which ones are         performant. 
                                                                                
#### Sorting                                                                    
<details>                                                                       
<summary>Click to expand</summary>                                              
                                                                                
Sort the array and then finding duplicate occurances.                           
</details>                                                                      
                                                                                
#### Hashing                                                                    
<details>                                                                       
<summary>Click to expand</summary>                                              
                                                                                
First, check to see if a value is in your table, if it is then you're done, otherwise insert it.
</details>                                                                      
                                                                                
</details>  

## Duplicate Pairs
<details>
<summary>Click to expand</summary>

This is basically a combination of some previous challenges.

</details>

## Challenge Challenge
<details>
<summary>Click to expand</summary>

See [Arbitrary-precision arithmetic](https://en.wikipedia.org/wiki/Arbitrary-precision_arithmetic).

</details>
