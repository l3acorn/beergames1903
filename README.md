# Beergames 1903

Beergames is an attempt to bring programmers together and be silly. A lot of us learn by doing and it can be helpful to see how other developers solve problems.

Will beergames make you a better programmer? ¯\\\_(ツ)\_/¯ Couldn't hurt. Looking for some exercises or inspiration to learn a new programming language? Beergames is good for that.

Beergames winners are traditionally gifted a beer. The original beergames were mostly about beer but we're older now and simply aren't as fun. That doesn't mean we can't still have beergames. 

## Challenges
See [challenges](/challenges/README.md).

## Hints
See [hints](/hints/README.md).

## Participation
1. Clone this repo.
1. Create a folder in [participants](/participants/) where your code will go. 
1. Create a branch from `master` where your changes willl go.
1. Create a merge request back into master.
