# Challenges

## Largest and Smallest
Given an unsorted array of integers, find the largest and smallest of the values.

#### Example
```bash
./largest-and-smallest < ../sample-data/sm_random.csv
(min,max) = (0,98)
```

## Missing in Array
Given an unsorted array of integers in the range `a` to `b`, with one missing value, find the missing value.

#### Example

```bash
./missing-in-array < ../sample-data/sm_random_missing.csv
22
```

## XOR Cipher
Given a ciphertext and key, compute the plain text. Of note, `ciphertext` and `key` are base64 encoded.

#### Example
```bash
./xor-cipher < ../sample-data/sm_xor.csv
Beer is the secret message.
```

## Duplicate in Array

Given an array of integers find the duplicated value. 

#### Example
```bash
./duplicate-in-array < ../sample-data/sm_random_duplicate.csv
22
```

## Duplicate Pairs
Given an input of characters, return the top two duplicates of pairs. E.g. given an input of `abcddeeffeeddee` return `ed`. If pairs are tied, ether are valid.

#### Example
```bash
./duplicate-pairs < ../sample-data/sm_random_characters
ET
```

## Challenge Challenge
Attempt the challenges that operate on inputs of `integers` using 512-bit integers.

